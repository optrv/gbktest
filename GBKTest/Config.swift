//
//  Config.swift
//  
//
//  Created by Garazd on 20.10.2019.
//

import UIKit

enum Config {
    
    static let googleMapsAPIKey = "AIzaSyBuaBJAgEvsNkvtLgPmGUltLXBrqKy9jLg"
    static let showTabBarVCSegue = "showTabBarVCSegue"
    static let listTableViewCell = "ListTableViewCell"
    static let listTableViewCellHeight: CGFloat = 90
    static let defalutMapCameraZoom: Float = 10.0
    static let markerImageName = "map-marker"
    
    // MARK: DB
    
    enum DB {
        static let dbCollectionName = "points"
        static let documentFieldName = "name"
        static let documentFieldCoordinate = "coordinate"
    }
    
    enum CoordIndex {
        static let latitude = 0
        static let longitude = 1
    }
    
    // MARK: Notifications
    
    enum Notification {
        static let signIn = "SignIn"
        static let signOut = "SignOut"
        static let dataHasBeenChanged = "DataHasBeenChanged"
    }
    
    
    // MARK: Alert Controller
    
    enum AlertController {
        static let ok = "OK"
        static let cancel = "Cancel"
        static let yes = "yes"
        static let no = "no"
        
        static let mapVCTitle = "Input the point's name"
        static let mapVCTextFieldPlaceHolder = "Point's name"
        static let mapVCMessage = "Please, choose another (unique) name!"
        
        static let listVCTitle = "Are you sure?"
        static let listVCMessage = "This action cannot be undone!"
    }
    
    // MARK: Etc
    
    enum TabBarVCIndex {
        static let listTableViewController = 0
        static let mapViewController = 1
        static let profileViewController = 2
    }
}
