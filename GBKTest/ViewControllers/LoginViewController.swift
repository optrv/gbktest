//
//  LoginViewController.swift
//  GBKTest
//
//  Created by Garazd on 16.10.2019.
//  Copyright © 2019 Garazd. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        NotificationCenter.default.addObserver(self, selector: #selector(showTabBarVC), name: Notification.Name(Config.Notification.signIn), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let appDelegate = appDelegate, appDelegate.userIsLoggedIn() {
            showTabBarVC()
        }
    }
    
    @objc private func showTabBarVC() {
        performSegue(withIdentifier: Config.showTabBarVCSegue, sender: self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
