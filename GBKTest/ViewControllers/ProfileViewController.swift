//
//  ProfileViewController.swift
//  GBKTest
//
//  Created by Garazd on 18.10.2019.
//  Copyright © 2019 Garazd. All rights reserved.
//

import UIKit

final class ProfileViewController: UIViewController {
    
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.signOut()
        dismiss(animated: true, completion: nil)
    }
}
