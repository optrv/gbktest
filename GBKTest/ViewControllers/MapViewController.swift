//
//  MapViewController.swift
//  GBKTest
//
//  Created by Garazd on 18.10.2019.
//  Copyright © 2019 Garazd. All rights reserved.
//

import UIKit
import GoogleMaps

final class MapViewController: UIViewController {
    
    private var locationManager = CLLocationManager()
    private var mapView: GMSMapView!
    private var currentMarker: GMSMarker!
    var selectedLocation: Point?
    private var markersDict = [String: GMSMarker]()
    private var zoom: Float = Config.defalutMapCameraZoom
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        setupMapView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupMarkers()
        moveToLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locationManager.stopUpdatingLocation()
    }
    
    private func setupLocationManager() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
    }
    
    private func setupMapView() {
        mapView = GMSMapView.map(withFrame: .zero, camera: GMSCameraPosition())
        self.view = mapView
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.animate(toZoom: zoom)
    }
    
    @objc private func setupMarkers() {
        DataManager.getPoints { [weak self] (done) in
            if done {
                for point in DataManager.pointArray {
                    let position = CLLocationCoordinate2D(latitude: point.coordinate[Config.CoordIndex.latitude], longitude: point.coordinate[Config.CoordIndex.longitude])
                    guard let markersDictValues = self?.markersDict.values else { return }
                    if !markersDictValues.contains(where: { (marker) -> Bool in
                        marker.position.latitude == position.latitude && marker.position.latitude == position.latitude
                    }) {
                        DispatchQueue.main.async {
                            self?.createMarker(position: position, name: point.name)
                        }
                    }
                }
            }
        }
    }
    
    private func createMarker(position: CLLocationCoordinate2D, name: String) {
        let marker = GMSMarker(position: position)
        let markerImage = UIImage(named: Config.markerImageName)
        let markerView = UIImageView(image: markerImage)
        markerView.tintColor = UIColor.red
        marker.iconView = markerView
        marker.title = name
        marker.map = self.mapView
        markersDict.updateValue(marker, forKey: name)
    }
    
    func removeMarker(point: Point) {
        let marker = markersDict[point.name]
        marker?.map = nil
        markersDict.removeValue(forKey: point.name)
    }
    
    private func moveToLocation() {
        if let selectedLocation = selectedLocation {
            let name = selectedLocation.name
            let coordinate = selectedLocation.coordinate
            let location = CLLocationCoordinate2D(latitude: coordinate[Config.CoordIndex.latitude], longitude: coordinate[Config.CoordIndex.longitude])
            mapView.animate(toLocation: location)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.currentMarker = self.markersDict[name] ?? GMSMarker()
                self.currentMarker.map = self.mapView
                self.mapView.selectedMarker = self.currentMarker
            }
            self.selectedLocation = nil
        } else {
            locationManager.startUpdatingLocation()
        }
    }
}

// MARK: Location Manager Delegate

extension MapViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let latitude = location.coordinate.latitude, longitude = location.coordinate.longitude
        let camera = GMSCameraPosition(latitude: latitude, longitude: longitude, zoom: zoom)
        mapView.camera = camera
        locationManager.stopUpdatingLocation()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      switch status {
      case .restricted:
        print("Location access was restricted.")
      case .denied:
        print("User denied access to location.")
        mapView.isHidden = false
      case .notDetermined:
        print("Location status not determined.")
      case .authorizedAlways: fallthrough
      case .authorizedWhenInUse:
        print("Location status is OK.")
      default: return
      }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print(error.localizedDescription)
    }
}

// MARK: GoogleMaps Delegate

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        var pointName = "", pointNameCapitalized = ""
        let alertController = UIAlertController(title: Config.AlertController.mapVCTitle, message: nil, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: Config.AlertController.ok, style: .default) { [weak self] (action) in
            let point = Point(name: pointNameCapitalized, coordinate: [coordinate.latitude, coordinate.longitude])
            DataManager.postPoints(point: point)
            self?.setupMarkers()
        }
        let cancelAction = UIAlertAction(title: Config.AlertController.cancel, style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        [confirmAction, cancelAction].forEach { (action) in
            alertController.addAction(action)
        }
        alertController.addTextField { [weak self] (textField) in
            textField.placeholder = Config.AlertController.mapVCTextFieldPlaceHolder
            confirmAction.isEnabled = false
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                pointName = alertController.textFields?[0].text ?? ""
                pointNameCapitalized = pointName.prefix(1).capitalized + pointName.dropFirst()
                guard let markerDictKeys = self?.markersDict.keys else { return }
                if !markerDictKeys.contains(where: { (name) -> Bool in
                    name == pointNameCapitalized
                }) {
                    confirmAction.isEnabled = textField.text!.count > 0
                    alertController.message = nil
                } else {
                    confirmAction.isEnabled = false
                    alertController.message = Config.AlertController.mapVCMessage
                }
            }
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        mapView.selectedMarker = nil
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        mapView.selectedMarker = nil
    }
}
