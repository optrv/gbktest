//
//  ListTableViewController.swift
//  GBKTest
//
//  Created by Garazd on 18.10.2019.
//  Copyright © 2019 Garazd. All rights reserved.
//

import UIKit

final class ListTableViewController: UIViewController {
    
    @IBOutlet weak var listTableView: UITableView!
    private var selectedLocation: [String : Any] = [:]
    private var longPressGestureRecognizer: UILongPressGestureRecognizer!
    private var mapViewController: MapViewController!
    private let mapViewVCIndex = Config.TabBarVCIndex.mapViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupGestureRecognizer()
        setupMapViewControllerLink()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: Notification.Name(Config.Notification.dataHasBeenChanged), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setupTableView() {
        listTableView.delegate = self
        listTableView.dataSource = self
        listTableView.rowHeight = Config.listTableViewCellHeight
        DataManager.getPoints { [weak self] (done) in
           if done {
               DispatchQueue.main.async {
                   self?.reloadData()
               }
           }
       }
    }
    
    @objc private func reloadData() {
        if DataManager.pointArray.isEmpty {
            listTableView.isHidden = true
        } else {
            listTableView.isHidden = false
        }
        listTableView.reloadData()
    }
    
    private func setupGestureRecognizer() {
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(cellDidLongPressed))
        longPressGestureRecognizer.delegate = self
        listTableView.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    private func setupMapViewControllerLink() {
        let mapNavController = self.tabBarController?.viewControllers![mapViewVCIndex] as? UINavigationController
        mapViewController = mapNavController?.topViewController as? MapViewController
    }
    
    private func selectedPoint(indexPath: IndexPath) -> Point {
        let selectedPoint = DataManager.pointArray[indexPath.row]
        let name = selectedPoint.name, coordinate = selectedPoint.coordinate
        return Point(name: name, coordinate: coordinate)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: Delegate extensions

extension ListTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.pointArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listTableView.dequeueReusableCell(withIdentifier: Config.listTableViewCell, for: indexPath) as! ListTableViewCell
        let name = "\(DataManager.pointArray[indexPath.row].name)"
        let latitude = "\(DataManager.pointArray[indexPath.row].coordinate[Config.CoordIndex.latitude])"
        let longitude = "\(DataManager.pointArray[indexPath.row].coordinate[Config.CoordIndex.longitude])"
        cell.setup(name: name, latitude: latitude, longitude: longitude)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mapViewController.selectedLocation = selectedPoint(indexPath: indexPath)
        tabBarController?.selectedIndex = mapViewVCIndex
    }
}

extension ListTableViewController: UIGestureRecognizerDelegate {
    @objc func cellDidLongPressed() {
        let pressLocation = longPressGestureRecognizer.location(in: self.listTableView)
        guard let indexPath = self.listTableView.indexPathForRow(at: pressLocation) else { return }
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
            let alertController = UIAlertController(title: Config.AlertController.listVCTitle, message: Config.AlertController.listVCMessage, preferredStyle: .alert)
            let yesAlertAction = UIAlertAction(title: Config.AlertController.yes, style: .destructive) { (action) in
                self.mapViewController.removeMarker(point: self.selectedPoint(indexPath: indexPath))
                DataManager.removePoint(indexPath: indexPath)
            }
            let noAlertAction = UIAlertAction(title: Config.AlertController.no, style: .cancel) { (action) in
                return
            }
            [yesAlertAction, noAlertAction].forEach { (action) in
                alertController.addAction(action)
            }
            present(alertController, animated: true, completion: nil)
        }
    }
}
