//
//  ListTableViewCell.swift
//  GBKTest
//
//  Created by Garazd on 18.10.2019.
//  Copyright © 2019 Garazd. All rights reserved.
//

import UIKit

final class ListTableViewCell: UITableViewCell {
    
    // MARK: Outlets

    @IBOutlet weak var pointNameLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(name: String, latitude: String, longitude: String) {
        pointNameLabel.text = name
        latitudeLabel.text = latitude
        longitudeLabel.text = longitude
    }
}
