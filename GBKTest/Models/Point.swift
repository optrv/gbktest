//
//  Point.swift
//  GBKTest
//
//  Created by Garazd on 20.10.2019.
//  Copyright © 2019 Garazd. All rights reserved.
//

import Foundation

struct Point {
    var id: String = ""
    var name: String = ""
    var coordinate: [Double] = [0]
}
