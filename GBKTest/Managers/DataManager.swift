//
//  DataManager.swift
//  GBKTest
//
//  Created by Garazd on 20.10.2019.
//  Copyright © 2019 Garazd. All rights reserved.
//

import Foundation
import Firebase

struct DataManager {
    
    static private let db = Firestore.firestore()
    static var pointArray = [Point]() {
        didSet {
            NotificationCenter.default.post(name: Notification.Name(Config.Notification.dataHasBeenChanged), object: nil)
        }
    }
    
    static func getPoints(completion: @escaping (Bool) -> Void) {
        pointArray.removeAll()
        let ordered = db.collection(Config.DB.dbCollectionName).order(by: Config.DB.documentFieldName)
        ordered.getDocuments { (querySnapshot, error) in
            if error == nil {
                for document in querySnapshot!.documents {
                    print(document)
                    let id = document.documentID
                    let name = document.data()[Config.DB.documentFieldName] as? String
                    let geopoint = document.data()[Config.DB.documentFieldCoordinate] as? GeoPoint
                    let coordinate = [geopoint?.latitude, geopoint?.longitude] as? [Double]
                    pointArray.append(Point(id: id, name: name!, coordinate: coordinate!))
                }
                completion(true)
            } else {
                print(error?.localizedDescription as Any)
                completion(false)
            }
        }
    }
    
    static func postPoints(point: Point) {
        var ref: DocumentReference?
        let name = point.name
        let coordinate = point.coordinate
        let geoPoint = GeoPoint(latitude: coordinate[Config.CoordIndex.latitude], longitude: coordinate[Config.CoordIndex.longitude])
        let dict: [String: Any] = [Config.DB.documentFieldName: name, Config.DB.documentFieldCoordinate: geoPoint]
        ref = db.collection(Config.DB.dbCollectionName).addDocument(data: dict, completion: { (error) in
            if error != nil {
                print(error?.localizedDescription as Any)
            } else {
                print(ref as Any)
            }
        })
    }
    
    static func removePoint(indexPath: IndexPath) {
        let id = pointArray[indexPath.row].id
        db.collection(Config.DB.dbCollectionName).document(id).delete { (error) in
            if error == nil {
                pointArray.remove(at: indexPath.row)
            }
        }
    }
}
